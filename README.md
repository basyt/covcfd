# CovCFD

An introduction to CFD programming

- [Exercise 0: Time integration with explicit Runge-Kutta methods](p0_explicit_time_integration/README.md)
    - _Subjects_: Initial value problems, time integration, order of accuracy
    - _Outcome_: Implement multiple time integrators for vector-valued functions and demonstrate their accuracy
- [Exercise 1: 1D advection](p0_1d_advection/p0_1d_advection.md)
    - _Subjects_: Finite difference approximations, central differencing, upwinding
    - _Outcome_: Numerical solution of 1D linear and nonlinear advection equations with varying spatial and temporal accuracy
