#pragma once

#include "ExplicitRungeKutta.hpp"

class ForwardEuler : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override { return Eigen::MatrixXd::Zero(1, 1); }
  Eigen::VectorXd ButcherTableauB() const override { return Eigen::VectorXd::Constant(1, 1.0); }
  Eigen::VectorXd ButcherTableauC() const override { return Eigen::VectorXd::Constant(1, 0.0); }
};
