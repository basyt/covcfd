#pragma once

#include "ExplicitRungeKutta.hpp"

class RK2Midpoint : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(2, 2);
    A(1, 0)           = 1.0;
    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(2);
    b[0]              = 0.5;
    b[1]              = 0.5;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(2);
    c[1]              = 1.0;
    return c;
  }
};