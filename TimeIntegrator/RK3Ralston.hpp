#pragma once

#include "ExplicitRungeKutta.hpp"

class RK3Ralston : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(3, 3);
    A(1, 0)           = 0.5;
    A(2, 1)           = 0.75;
    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(3);
    b[0]              = 2. / 9.;
    b[1]              = 1. / 3.;
    b[2]              = 4. / 9.;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(3);
    c[1]              = 0.5;
    c[2]              = 0.75;
    return c;
  }
};
