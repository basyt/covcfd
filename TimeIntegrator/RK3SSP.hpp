#pragma once

#include "ExplicitRungeKutta.hpp"

class RK3SSP : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(3, 3);
    A(1, 0)           = 1.0;
    A(2, 0)           = 0.25;
    A(2, 1)           = 0.25;
    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(3);
    b[0]              = 1. / 6.;
    b[1]              = 1. / 6.;
    b[2]              = 2. / 3.;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(3);
    c[1]              = 1.0;
    c[2]              = 0.5;
    return c;
  }
};
