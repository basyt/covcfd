#pragma once

#include "ExplicitRungeKutta.hpp"

class RK4 : public ExplicitRungeKutta
{
private:
  Eigen::MatrixXd ButcherTableauA() const override
  {
    Eigen::MatrixXd A = Eigen::MatrixXd::Zero(4, 4);
    A(1, 0)           = 0.5;
    A(2, 1)           = 0.5;
    A(3, 2)           = 1.0;

    return A;
  }

  Eigen::VectorXd ButcherTableauB() const override
  {
    Eigen::VectorXd b = Eigen::VectorXd::Zero(4);
    b[0]              = 1. / 6.;
    b[1]              = 1. / 3.;
    b[2]              = 1. / 3.;
    b[3]              = 1. / 6.;
    return b;
  }

  Eigen::VectorXd ButcherTableauC() const override
  {
    Eigen::VectorXd c = Eigen::VectorXd::Zero(4);
    c[1]              = 0.5;
    c[2]              = 0.5;
    c[3]              = 1.0;
    return c;
  }
};