# Problem 1: An initial pass

- [Problem 1: An initial pass](p0_problem1.md)
- [Problem 1: Refactoring the time integrators](p0_problem1_refactoring.md)
- [Problem 1: Refactoring the time integrators part 2](p0_problem1_refactoring2.md)
- [Problem 2: An initial pass](p0_problem2.md)
- [Problem 2: Implementing the observer pattern](p0_problem2_observer.md)
- [Problem 2: Cleanup](p0_problem2_cleanup.md)


Working from an ubuntu 18.04 system with common build tools, hopefully the following successfully compiles this example. Adjust paths however you like.

```bash
mkdir ~/dev && cd ~/dev
git clone git@gitlab.com:Overunderrated/covcfd.git
cd covcfd
git checkout v0.1.1  # check out a git tag specific to this example
mkdir ../build && cd ../build
cmake ../covcfd
make
```

## Build preliminaries


Let's make a CMakeLists.txt file with a little bit of boilerplate and our own source file p0.cpp. We will try to minimize external dependencies, but two header-only libraries will be useful here:
- [Eigen](http://eigen.tuxfamily.org), a very capable template-based linear algebra library.
- [matplotlibcpp](https://github.com/lava/matplotlib-cpp), a C++ front-end to python's matplotlib. Only a subset of matplotlib's functionality but it's an easy option for simple graphical line plots.


<details>
<summary><b>Click to expand CMakeLists.txt</b></summary>

```cmake
cmake_minimum_required(VERSION 3.10)

project(covcfd)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

if(NOT CMAKE_BUILD_TYPE)
  set(CMAKE_BUILD_TYPE Release)
endif()

set(CMAKE_CXX_FLAGS "-Wall -Wextra")
set(CMAKE_CXX_FLAGS_DEBUG "-g")
set(CMAKE_CXX_FLAGS_RELEASE "-O3")


# for matplotlibcpp
find_package(PythonLibs 2.7 REQUIRED)
add_definitions(-DWITHOUT_NUMPY)


include(ExternalProject)

ExternalProject_Add(eigen_repo
  GIT_REPOSITORY "https://gitlab.com/libeigen/eigen.git"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
)
set(EIGEN_INCLUDE_DIR ${CMAKE_BINARY_DIR}/eigen_repo-prefix/src/eigen_repo)

ExternalProject_Add(matplotlibcpp_repo
  GIT_REPOSITORY "https://github.com/lava/matplotlib-cpp.git"
  CONFIGURE_COMMAND ""
  BUILD_COMMAND ""
  INSTALL_COMMAND ""
)
set(MATPLOTLIB_INCLUDE_DIR ${CMAKE_BINARY_DIR}/matplotlibcpp_repo-prefix/src/matplotlibcpp_repo)

add_executable(p0 p0_explicit_time_integration/p0.cpp)
add_dependencies(p0 eigen_repo matplotlibcpp_repo)
target_include_directories(p0 PRIVATE ${EIGEN_INCLUDE_DIR} ${PYTHON_INCLUDE_DIRS} ${MATPLOTLIB_INCLUDE_DIR})
target_link_libraries(p0 ${PYTHON_LIBRARIES})
```

</details>


## Plot the exact solution

We know the exact solution for this problem, so test that our external libraries work and take a look at the solution. We define `int main()` and three helper functions:
- `double scalarODEExactSolution(double t)` which returns the exact solution at $`t`$,
- `void plotScalarODEExactSolution()` which calls `matplotlibcpp` to do the plotting,
- `std::vector<double> EigenToStdVec(const Eigen::VectorXd&)`, a helper function that converts Eigen vectors to `std::vector` so `matplotlibcpp` can plot them.

```c++
#include <Eigen/Dense>
#include <cmath>
#include <matplotlibcpp.h>
#include <vector>

namespace plt = matplotlibcpp;

std::vector<double>
EigenToStdVec(const Eigen::VectorXd& e)
{
  std::vector<double> v(e.rows());
  for (long i = 0; i < e.rows(); ++i) {
    v[i] = e[i];
  }
  return v;
}

double
scalarODEExactSolution(double t)
{
  return 1 + 12 * t - 10 * t * t + 0.5 * sin(20 * M_PI * std::pow(t, 3));
}

void
plotScalarODEExactSolution()
{
  const Eigen::VectorXd t = Eigen::VectorXd::LinSpaced(1000, 0, 1);
  plt::figure();
  plt::plot(EigenToStdVec(t), [](double t) { return scalarODEExactSolution(t); }, "k-");
  plt::xlabel("$t$");
  plt::ylabel("$u(t)$");
}

int
main()
{
  plotScalarODEExactSolution();
  plt::show();
}
```

Run `make` from your build directory, and hopefully you should see something like this:

<details>
<summary><b>Click to expand example solution plot</b></summary>
![alt text](levequesoln.png)
</details>


## Define interfaces and the right-hand-side of problem 1

Interfaces define how logically distinct parts of code interact, so it's worth it to give some thought. In this case we know we will deal with right-hand-sides that mathematically have the general form $`\mathbf{f}(t,\mathbf{u})`$ so a reasonable place to start would be a function with a similar function signature, like
```c++
void f(const Eigen::VectorXd& u, double time, Eigen::VectorXd& dudt)
```
where the ordering reflects a common C/C++ style of having the first parameters be inputs and the last parameters be modifiable input/outputs. For convenience define a type alias for this:
```c++
using RHSFunction = std::function<void(const Eigen::VectorXd&, Eigen::VectorXd&, double)>;
```
This problem has a source term, or right-hand-side of
```math
s(t) = 12 - 20t + 30 \pi t^2 \cos{(20 \pi t^3)}
```
so implement that function:
```c++
void scalarODERightHandSide(const Eigen::VectorXd& u, double t, Eigen::VectorXd& dudt)
{
  assert(u.size() == 1);
  dudt.resize(u.size());
  dudt[0] = 12 - 20 * t + 30 * M_PI * t * t * cos(20 * M_PI * std::pow(t, 3));
}
```

## Define the integrator interface

The time integrators implemented here only require a few pieces of input information:
- The right hand side function
- The initial condition
- The initial time, final time, and time step size
and then they should return the solution. A reasonable first attempt could look like
```c++
Eigen::VectorXd integrator(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
{
    // do work
}
```

## Define the forward Euler integrator

Starting with the simplest forward Euler (RK1) integrator, its implementation can look like this, noting the (intentionally excessive) commenting here:
```c++
Eigen::VectorXd forward_euler(const RHSFunction& rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
{
  Eigen::VectorXd u    = u0; // set the solution to the initial value
  double          time = t0; // set the time to the initial time
  Eigen::VectorXd dudt;      // allocate a vector for the update

  while (time < tf) { // iterate until final time is reached
    rhs(u, time, dudt); // get the right-hand-side for this problem

    // ensure that we end exactly at the final time tf by adjusting
    // the size of the last step
    if (time + dt >= tf) {
      dt   = tf - time;
      time = tf;
    } else {
      time += dt;
    }

    u += dt * dudt; // increment the solution
  }

  return u; // return the solution
}
```
and that's it!


## See if it all works

For utility, define the relative error function
```c++
double relative_error(double expected, double computed)
{
  return abs(computed / expected - 1.0);
}
```
and modify the `int main()` function to use time step sizes of $`10^{-2}, 10^{-3}, 10^{-4}`$:
```c++
int main()
{
  plotScalarODEExactSolution();
  const Eigen::VectorXd u0 = Eigen::VectorXd::Constant(1, 1.0);
  const double          t0 = 0;
  const double          tf = 1;
  const double exact_soln  = scalarODEExactSolution(tf);

  const std::vector<double> timestepsize{ 1. / 100., 1. / 1000., 1. / 10000. };
```
then iterate through the time step values:
```c++
  std::vector<double> forward_euler_err;
  for (const auto dt : timestepsize) {
    // get the solution at tf using time step dt
    const auto forward_euler_soln = forward_euler(scalarODERightHandSide, u0, t0, tf, dt);
    // compute the relative error
    const double rel_err = relative_error(exact_soln, forward_euler_soln[0]);
    // store the relative error for subsequent plotting
    forward_euler_err.push_back(rel_err);
  }
```
and then plot the result:
```c++
  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Error");
  plt::show();
}
```
If all goes well, you should get a plot that looks like this:

<details>
<summary><b>Click to expand error vs timestep for forward Euler</b></summary>
![alt text](levequeforwardeulerconvergence.png)
</details>

the smallest time step $`10^{-4}`$ is resulting in a relative error of about $`10^{-3}`$, not bad!

## Implement RK4

Since our interfaces are sanely defined, implementing 4th order 4-stage Runge-Kutta (RK4), also know as *the* Runge-Kutta algorithm as it was one of the first used in practice (by Carl Runge and Wilhelm Kutta), should be straightforward.

The structure of the code looks largely the same. A major difference is that we now have 4 calls to the right hand side, and 4 temporary solution vectors, as compared to 1 for the forward Euler method.

<details>
<summary><b>Click to expand the RK4 code</b></summary>

```c++
Eigen::VectorXd rk4(RHSFunction rhs, const Eigen::VectorXd& u0, double t0, double tf, double dt)
{
  Eigen::VectorXd u    = u0;
  double          time = t0;

  Eigen::VectorXd k1, k2, k3, k4;

  while (time < tf) {
    rhs(u, time, k1);
    rhs(u + 0.5 * k1, time + 0.5 * dt, k2);
    rhs(u + 0.5 * k2, time + 0.5 * dt, k3);
    rhs(u + k3, time + dt, k4);

    if (time + dt >= tf) {
      dt   = tf - time;
      time = tf;
    } else {
      time += dt;
    }

    u += 1. / 6. * dt * (k1 + 2. * k2 + 2. * k3 + k4);
  }

  return u;
}
```

</details>

modify `int main()` to run the same time step sweep using RK4:



<details>
<summary><b>Click to expand the main function code</b></summary>

```c++
int main()
{
  plotScalarODEExactSolution();

  const Eigen::VectorXd u0 = Eigen::VectorXd::Constant(1, 1.0);
  const double          t0 = 0;
  const double          tf = 1;

  const double exact_soln = scalarODEExactSolution(tf);

  const std::vector<double> timestepsize{ 1. / 100., 1. / 1000., 1. / 10000. };
  std::vector<double>       forward_euler_err;
  std::vector<double>       rk4_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(
      relative_error(exact_soln, forward_euler(scalarODERightHandSide, u0, t0, tf, dt)[0]));

    rk4_err.push_back(
      relative_error(exact_soln, rk4(scalarODERightHandSide, u0, t0, tf, dt)[0]));
  }

  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Error");
  plt::show();
}
```

</details>

and if it all worked you should see this:

![alt text](levequerk1rk4convergence.png)

where forward Euler had an error of $`\approx 10^{-3}`$, RK4 has an error of $`< 10^{-12}`$, more than 9 orders of magnitude better! Not only is the error itself better for a given fixed time step, but specifically the *rate of convergence* is dramatically better. Reducing the time step by a factor of 2 lowers the error in Forward Euler by a factor of $`2^1`$, whereas for RK4 it lowers the error by a factor of $`2^4=16`$.


Before continuing on and implementing more integrators, [we can improve on what we have to make things easier on ourselves in the future.](p0_problem1_refactoring.md)