#include <Eigen/Dense>
#include <cmath>
#include <matplotlibcpp.h>
#include <memory>
#include <vector>

#include "TimeIntegrator/ForwardEuler.hpp"
#include "TimeIntegrator/RK2Midpoint.hpp"
#include "TimeIntegrator/RK3SSP.hpp"
#include "TimeIntegrator/RK3Ralston.hpp"
#include "TimeIntegrator/RK4.hpp"


namespace plt     = matplotlibcpp;
using RHSFunction = std::function<void(const Eigen::VectorXd&, double, Eigen::VectorXd&)>;

std::vector<double>
EigenToStdVec(const Eigen::VectorXd& e)
{
  std::vector<double> v(e.rows());
  for (long i = 0; i < e.rows(); ++i) {
    v[i] = e[i];
  }
  return v;
}

double
scalarODEExactSolution(double t)
{
  return 1 + 12 * t - 10 * t * t + 0.5 * sin(20 * M_PI * std::pow(t, 3));
}

void
plotScalarODEExactSolution()
{
  const Eigen::VectorXd t = Eigen::VectorXd::LinSpaced(1000, 0, 1);
  plt::figure();
  plt::plot(EigenToStdVec(t), [](double t) { return scalarODEExactSolution(t); }, "k-");
  plt::xlabel("$t$");
  plt::ylabel("$u(t)$");
}

void
scalarODERightHandSide(const Eigen::VectorXd& u, double t, Eigen::VectorXd& dudt)
{
  assert(u.size() == 1);
  dudt.resize(u.size());
  dudt[0] = 12 - 20 * t + 30 * M_PI * t * t * cos(20 * M_PI * std::pow(t, 3));
}

double
relative_error(double expected, double computed)
{
  return abs(computed / expected - 1.0);
}

void
solveProblem1ScalarODE()
{
  plotScalarODEExactSolution();

  const Eigen::VectorXd u0 = Eigen::VectorXd::Constant(1, 1.0);
  const double          t0 = 0;
  const double          tf = 1;

  const double exact_soln = scalarODEExactSolution(tf);

  const std::vector<double> timestepsize{ 1. / 100., 1. / 1000., 1. / 10000. };
  std::vector<double>       rk4_err;
  std::vector<double>       rk3ssp_err;
  std::vector<double>       rk3ralston_err;
  std::vector<double>       rk2_err;
  std::vector<double>       forward_euler_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(
      relative_error(exact_soln, ForwardEuler().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk4_err.push_back(
      relative_error(exact_soln, RK4().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk3ssp_err.push_back(
      relative_error(exact_soln, RK3SSP().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk3ralston_err.push_back(
      relative_error(exact_soln, RK3Ralston().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
    rk2_err.push_back(
      relative_error(exact_soln, RK2Midpoint().solve(scalarODERightHandSide, u0, t0, tf, dt)[0]));
  }

  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::named_loglog("RK2", timestepsize, rk2_err, "-o");
  plt::named_loglog("RK3 SSP", timestepsize, rk3ssp_err, "-o");
  plt::named_loglog("RK3 Ralston", timestepsize, rk3ralston_err, "-o");
  plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Error");
}


int
main()
{
  solveProblem1ScalarODE();

  plt::show();
}
