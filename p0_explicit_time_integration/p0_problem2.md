# Problem 2: An initial pass

- [Problem 1: An initial pass](p0_problem1.md)
- [Problem 1: Refactoring the time integrators](p0_problem1_refactoring.md)
- [Problem 1: Refactoring the time integrators part 2](p0_problem1_refactoring2.md)
- [Problem 2: An initial pass](p0_problem2.md)
- [Problem 2: Implementing the observer pattern](p0_problem2_observer.md)
- [Problem 2: Cleanup](p0_problem2_cleanup.md)

Run
```
git checkout v0.2.1
```
to get to the code at the end of this page.

## New thing same as the old thing

Similarly to the right hand side function of the previous problem, let's implement the Lotka-Volterra right hand side with the same signature:

<details>
<summary><b>Click to expand Lotka-Volterra right hand side</b></summary>


```c++
void lotkaVolterraRightHandSide(const Eigen::VectorXd& u, double t, Eigen::VectorXd& dudt)
{
  constexpr double alpha = 1.1;
  constexpr double beta  = 0.4;
  constexpr double gamma = 0.1;
  constexpr double delta = 0.4;
  assert(u.size() == 2);
  dudt.resize(u.size());
  dudt[0] = alpha * u[0] - beta * u[0] * u[1];
  dudt[1] = delta * u[0] * u[1] - gamma * u[1];
}
```

</details>

simple enough. We also know we're interested in the quantity `$V$` that *should* be conserved, so write a function to evaluate that term based on the solution at any given instant:


<details>
<summary><b>Click to expand Lotka-Volterra conserved term</b></summary>

```c++
double lotkaVolterraConservedConstant(const Eigen::VectorXd& u)
{
  // no, you really shouldn't be repeating magic constants in code like this.
  constexpr double alpha = 1.1;
  constexpr double beta  = 0.4;
  constexpr double gamma = 0.1;
  constexpr double delta = 0.4;
  return delta * u[0] - gamma * log(u[0]) + beta * u[1] - alpha * log(u[1]);
}
```

</details>


Analogous to problem 1, loop through some time step sizes and check the errors. In this instance we don't have an exact *solution* to compare to, so instead call the `lotkaVolterraConservedConstant` and compare that to the value computed at the initial condition.



<details>
<summary><b>Click to expand Lotka-Volterra solution function</b></summary>

```c++
void solveProblem2LotkaVolterra()
{
  constexpr double t0 = 0;
  constexpr double tf = 100;
  Eigen::VectorXd  u0 = Eigen::VectorXd::Constant(2, 10.0);
  const double     V0 = lotkaVolterraConservedConstant(u0);

  const std::vector<double> timestepsize{ tf / 1.e5, tf / 1.e6, tf / 1.e7 };
  std::vector<double>       rk4_err;
  std::vector<double>       rk3ssp_err;
  std::vector<double>       rk3ralston_err;
  std::vector<double>       rk2_err;
  std::vector<double>       forward_euler_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        ForwardEuler().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk2_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK2Midpoint().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk3ssp_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK3SSP().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk3ralston_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK3Ralston().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk4_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK4().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
  }

  plt::figure();
  plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
  plt::named_loglog("RK2", timestepsize, rk2_err, "-o");
  plt::named_loglog("RK3 SSP", timestepsize, rk3ssp_err, "-o");
  plt::named_loglog("RK3 Ralston", timestepsize, rk3ralston_err, "-o");
  plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
  plt::legend();
  plt::grid(true);
  plt::xlabel("$\\Delta t$");
  plt::ylabel("Relative error in $V$");
}

int main()
{
  // solveProblem1ScalarODE();
  solveProblem2LotkaVolterra();

  plt::show();
}
```

</details>

and you should produce a figure that looks something like this:


![alt text](lotkavolterraVconvergence.png)


Curiously it is apparent all the integrators have reduced to 1st order accuracy on this problem, but why? Possibly due to some singularity in the derivatives at the very abruptly changing peaks seen in the function.

It also turns out [the Lotka-Volterra system is an example of a Hamiltonian system.](https://en.wikipedia.org/wiki/Hamiltonian_mechanics) Hamiltonian systems have particular conservation properties (which explains why there is this conserved quantity `$V$` to begin with) which the usual Runge-Kutta methods don't conserve. The same is true with other Hamiltonian systems, e.g. the kinds of problems that come up in orbital mechanics. For Hamiltonian problems [the special class of "symplectic integrators" should be used](https://en.wikipedia.org/wiki/Symplectic_integrator), particularly when the interest is on long-time behavior.

It also turns out that non-symplectic *error adaptive* variable time-step explicit Runge-Kutta methods can do very well for this problem.


Since specifying a constant time step size is not meaningful for an adaptive integrator, here it's plotted against $`t_f / nsteps`$, the average time step size. We've used the [Runge-Kutta-Fehlberg 7(8) (RKF78)](https://www.boost.org/doc/libs/1_72_0/libs/numeric/odeint/doc/html/boost/numeric/odeint/runge_kutta_fehlberg78.html) method and the [Cash-Karp](https://en.wikipedia.org/wiki/Cash%E2%80%93Karp_method) 4(5) method.

![alt text](lotkavolterraVconvergenceadaptive.png)


Error-adaptive methods are pretty useful. It should be clear they can be dramatically more efficient, and in this particular case they manage to maintain their formal order of accuracy while the other RK methods do not.

Some hacked together code to quickly hook into the `boost::numeric::odeint` interfaces below.


<details>
<summary><b>Click to expand Lotka-Volterra using adaptive time integrators</b></summary>

```c++

void
solveProblem2LotkaVolterra()
{
  constexpr double      t0 = 0;
  constexpr double      tf = 1000;
  const Eigen::VectorXd u0 = Eigen::VectorXd::Constant(2, 10.0);
  const double          V0 = lotkaVolterraConservedConstant(u0);

  const std::vector<double> timestepsize{ tf / 1.e5, tf / 1.e6, tf / 1.e7 };
  std::vector<double>       rk4_err;
  std::vector<double>       rk3ssp_err;
  std::vector<double>       rk3ralston_err;
  std::vector<double>       rk2_err;
  std::vector<double>       forward_euler_err;

  for (const auto dt : timestepsize) {
    forward_euler_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        ForwardEuler().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk2_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK2Midpoint().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk3ssp_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(RK3SSP().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk3ralston_err.push_back(relative_error(
      V0,
      lotkaVolterraConservedConstant(
        RK3Ralston().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
    rk4_err.push_back(relative_error(
      V0, lotkaVolterraConservedConstant(RK4().solve(lotkaVolterraRightHandSide, u0, t0, tf, dt))));
  }

  const std::vector<double> rel_err = { 1.e-2, 1.e-4, 1.e-6, 1.e-8, 1.e-10, 1.e-12 };
  std::vector<double>       rkck54_err;
  std::vector<double>       rkck54_step_size;

  for (const auto errtol : rel_err) {
    using namespace boost::numeric::odeint;
    typedef std::vector<double> state_type;

    // Cash-Karp 54

    typedef runge_kutta_cash_karp54<state_type>               error_stepper_type_rkck54;
    typedef controlled_runge_kutta<error_stepper_type_rkck54> controlled_stepper_type_rkck54;

    const double                   a_x    = 1;
    const double                   a_dxdt = 1;
    controlled_stepper_type_rkck54 controlled_stepper_rkck54(
      default_error_checker<double, range_algebra, default_operations>(0, errtol, a_x, a_dxdt));

    std::vector<double> uboost = EigenToStdVec(u0);
    size_t              steps  = integrate_adaptive(
      controlled_stepper_rkck54,
      [](const state_type& x, state_type& dxdt, double t) {
        auto            eig_x = StdVecToEigen(x);
        Eigen::VectorXd eig_dxdt;
        lotkaVolterraRightHandSide(eig_x, t, eig_dxdt);
        dxdt = EigenToStdVec(eig_dxdt);
      },
      uboost,
      t0,
      tf,
      errtol);
    const auto uboostsoln = StdVecToEigen(uboost);
    rkck54_err.push_back(relative_error(V0, lotkaVolterraConservedConstant(uboostsoln)));
    rkck54_step_size.push_back(double(tf) / double(steps));
  }

    std::vector<double> rkf78_err;
    std::vector<double> rkf78_step_size;

    for (const auto errtol : rel_err) {
      using namespace boost::numeric::odeint;
      typedef std::vector<double> state_type;

      // RKF 78

      typedef runge_kutta_fehlberg78<state_type>               error_stepper_type_rkf78;
      typedef controlled_runge_kutta<error_stepper_type_rkf78> controlled_stepper_type_rkf78;

      const double                  a_x    = 1;
      const double                  a_dxdt = 1;
      controlled_stepper_type_rkf78 controlled_stepper_rkf78(
        default_error_checker<double, range_algebra, default_operations>(
          0, errtol, a_x, a_dxdt));

      std::vector<double> uboost_rkf78 = EigenToStdVec(u0);
      size_t              steps  = integrate_adaptive(
        controlled_stepper_rkf78,
        [](const state_type& x, state_type& dxdt, double t) {
          auto            eig_x = StdVecToEigen(x);
          Eigen::VectorXd eig_dxdt;
          lotkaVolterraRightHandSide(eig_x, t, eig_dxdt);
          dxdt = EigenToStdVec(eig_dxdt);
        },
        uboost_rkf78,
        t0,
        tf,
        errtol);
      const auto uboostsoln_rkf78 = StdVecToEigen(uboost_rkf78);
      rkf78_err.push_back(relative_error(V0, lotkaVolterraConservedConstant(uboostsoln_rkf78)));
      rkf78_step_size.push_back(double(tf) / double(steps));
    }

    plt::figure();
    plt::named_loglog("RK1 (Forward Euler)", timestepsize, forward_euler_err, "-o");
    plt::named_loglog("RK2", timestepsize, rk2_err, "-o");
    plt::named_loglog("RK3 SSP", timestepsize, rk3ssp_err, "-o");
    plt::named_loglog("RK3 Ralston", timestepsize, rk3ralston_err, "-o");
    plt::named_loglog("RK4", timestepsize, rk4_err, "-o");
    plt::named_loglog("Adaptive Cash-Karp RK54", rkck54_step_size, rkck54_err, "-o");
    plt::named_loglog("Adaptive RK-Fehlberg RKF78", rkf78_step_size, rkf78_err, "-o");
    plt::legend();
    plt::grid(true);
    plt::xlabel("$\\Delta t$");
    plt::ylabel("Relative error in $V$");
  }
```


[Problem 2: Implementing the observer pattern](p0_problem2_observer.md)