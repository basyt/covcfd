# Problem 2: Cleanup

- [Problem 1: An initial pass](p0_problem1.md)
- [Problem 1: Refactoring the time integrators](p0_problem1_refactoring.md)
- [Problem 1: Refactoring the time integrators part 2](p0_problem1_refactoring2.md)
- [Problem 2: An initial pass](p0_problem2.md)
- [Problem 2: Implementing the observer pattern](p0_problem2_observer.md)
- [Problem 2: Cleanup](p0_problem2_cleanup.md)

Run
```
git checkout v0.2.3
```
to fast-forward to the code at the end of this section.


## Organization

Nothing exciting here, but it should be fairly clear that you don't want to throw all this code into one big file. Move the time integrators into a separate `TimeIntegrator` directory and each integrator into their own file. In the main function, split it into two executables, one for each problem.